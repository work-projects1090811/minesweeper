package ru.eugenegorbunoff.minesweeper.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.eugenegorbunoff.minesweeper.GameHolder;
import ru.eugenegorbunoff.minesweeper.components.GameInstance;
import ru.eugenegorbunoff.minesweeper.service.dto.FieldInteractDTO;
import ru.eugenegorbunoff.minesweeper.service.dto.NewGameDTO;
import ru.eugenegorbunoff.minesweeper.service.response.ErrorResponse;
import ru.eugenegorbunoff.minesweeper.service.response.GameFieldResponse;
import ru.eugenegorbunoff.minesweeper.service.response.IResponse;


/**
 * Main REST API module defines existing pages and processed incoming requests
 */
@RestController
@CrossOrigin(origins = "https://minesweeper-test.studiotg.ru")
public class GameService {

    @PostMapping("/new")
    public ResponseEntity<IResponse> newGame(@RequestBody NewGameDTO dto) {
        GameInstance newInstance;
        try {
            newInstance = GameInstance.create(dto.getWidth(), dto.getHeight(), dto.getMines_count());
            GameHolder.addGame(newInstance.gameId, newInstance);
        } catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(GameFieldResponse.from(newInstance), HttpStatus.OK);
    }

    @PostMapping("/turn")
    public ResponseEntity<IResponse> interact(@RequestBody FieldInteractDTO dto) {
        GameInstance gameInstance;
        try {
            gameInstance = GameHolder.getGame(dto.getGame_id());
            gameInstance.interact(dto.getCol(), dto.getRow());
        } catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(GameFieldResponse.from(gameInstance), HttpStatus.OK);
    }

}
