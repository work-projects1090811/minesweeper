package ru.eugenegorbunoff.minesweeper.service.dto;

import lombok.Data;

import java.util.UUID;

/**
 * Representation of player interaction request. Variables uses snake case to match API reference
 */
@Data
public class FieldInteractDTO {
    private final UUID game_id;
    private final int col, row;
}
