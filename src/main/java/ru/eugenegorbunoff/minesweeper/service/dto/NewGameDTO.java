package ru.eugenegorbunoff.minesweeper.service.dto;

import lombok.Data;

/**
 * Representation of new game start request. Variables uses snake case to match API reference
 */
@Data
public class NewGameDTO {
    private final int width, height, mines_count;
}
