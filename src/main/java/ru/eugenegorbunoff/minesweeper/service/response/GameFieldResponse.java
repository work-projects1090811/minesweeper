package ru.eugenegorbunoff.minesweeper.service.response;

import ru.eugenegorbunoff.minesweeper.components.GameInstance;

import java.util.UUID;

/**
 * Response which will be returned on interaction success. Contains game parameters (id, width, height, mines count, game status and game field)
 */
public record GameFieldResponse(UUID game_id, int width, int height, int mines_count, boolean completed, String[][] field)
        implements IResponse {

    /**
     * Static method which transform GameInstance into GameFieldResponse
     * @param gameInstance instance to transform
     * @return response
     */
    public static GameFieldResponse from(GameInstance gameInstance) {
        return new GameFieldResponse(
                gameInstance.gameId,
                gameInstance.gameParams.fieldWidth, gameInstance.gameParams.fieldHeight, gameInstance.gameParams.minesCount,
                gameInstance.isCompleted(),
                gameInstance.gameField.getGameFieldAsArray()
        );
    }
}
