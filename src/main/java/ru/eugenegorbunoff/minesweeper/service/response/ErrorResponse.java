package ru.eugenegorbunoff.minesweeper.service.response;

/**
 * Response which will be returned on interaction fail. Contains error message
 */
public class ErrorResponse implements IResponse {
    public final String error;

    public ErrorResponse(Exception e) {
        this(e.toString());
        e.printStackTrace();
    }

    public ErrorResponse() {
        this("Произошла непредвиденная ошибка");
    }

    private ErrorResponse(String text) {
        this.error = text;
    }
}
