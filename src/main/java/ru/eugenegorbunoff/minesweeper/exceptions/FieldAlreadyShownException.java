package ru.eugenegorbunoff.minesweeper.exceptions;

/**
 * Exception called when player tried to interact with already shown cell
 */
public class FieldAlreadyShownException extends Exception {
    final int col, row;

    public FieldAlreadyShownException(int col, int row) {
        super();
        this.col = col;
        this.row = row;
    }

    @Override
    public String toString() {
        return String.format("Ошибка: нельзя взаимодействовать с полем [%d; %d], которое уже открыто", row, col);
    }
}
