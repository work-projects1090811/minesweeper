package ru.eugenegorbunoff.minesweeper.exceptions;

import java.util.UUID;


/**
 * Exception called when you tried to add game into pool when game is already added
 */
public class GameAlreadyExistsException extends Exception {
    final UUID uuid;

    public GameAlreadyExistsException(UUID uuid) {
        super();
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return String.format("Произошла внутренняя ошибка: не удалось создать игру, поскольку игра с таким ID уже существует (uuid=%s)", uuid.toString());
    }
}
