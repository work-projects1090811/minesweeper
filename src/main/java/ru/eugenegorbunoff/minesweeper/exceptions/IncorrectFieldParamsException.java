package ru.eugenegorbunoff.minesweeper.exceptions;


/**
 * Exception called when player tried to start the game with incorrect field params
 */
public class IncorrectFieldParamsException extends Exception {

    final int width, height, mines;
    public IncorrectFieldParamsException(int width, int height, int mines) {
        super();
        this.width = width;
        this.height = height;
        this.mines = mines;
    }

    @Override
    public String toString() {
        if (width <= 0 || height <= 0)
            return String.format("Ошибка: невозможно создать поле {%d x %d}", height, width);
        if (mines <= 0)
            return "Ошибка: количество мин на поле должно быть положительным";
        return String.format(
                "Ошибка: слишком большое количество мин. На поле должна быть по крайней мере одна ячейка без мины. Для поля {%d x %d} допустимо максимум %d, введено %d",
                height, width, height * width - 1, mines
        );
    }

}
