package ru.eugenegorbunoff.minesweeper.exceptions;


/**
 * Exception called when player sent incorrect request
 */
public class IncorrectRequestException extends Exception {
    final String reason;

    public IncorrectRequestException(String reason) {
        super();
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "Ошибка: некорректный запрос. " + reason;
    }
}
