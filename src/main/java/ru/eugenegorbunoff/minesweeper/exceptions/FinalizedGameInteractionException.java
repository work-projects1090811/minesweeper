package ru.eugenegorbunoff.minesweeper.exceptions;


/**
 * Exception called when player tried to interact with game field when game is over already
 */
public class FinalizedGameInteractionException extends Exception {

    @Override
    public String toString() {
        return "Ошибка: вы не можете взаимодействовать с полем после окончания игры";
    }
}
