package ru.eugenegorbunoff.minesweeper.components;

import ru.eugenegorbunoff.minesweeper.exceptions.IncorrectFieldParamsException;

/**
 * Simple game parameters holder
 */
public class GameParams {
    public final int fieldHeight, fieldWidth, minesCount;
    public GameParams(int fieldHeight, int fieldWidth, int minesCount) throws IncorrectFieldParamsException {
        if (minesCount <= 0 || fieldHeight <= 0 || fieldWidth <= 0 || minesCount >= fieldHeight * fieldWidth)
            throw new IncorrectFieldParamsException(fieldWidth, fieldHeight, minesCount);

        this.fieldHeight = fieldHeight;
        this.fieldWidth = fieldWidth;
        this.minesCount = minesCount;
    }
}
