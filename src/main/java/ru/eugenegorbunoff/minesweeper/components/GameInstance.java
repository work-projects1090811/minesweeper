package ru.eugenegorbunoff.minesweeper.components;

import lombok.Getter;
import ru.eugenegorbunoff.minesweeper.exceptions.FieldAlreadyShownException;
import ru.eugenegorbunoff.minesweeper.exceptions.FinalizedGameInteractionException;
import ru.eugenegorbunoff.minesweeper.exceptions.IncorrectFieldParamsException;
import ru.eugenegorbunoff.minesweeper.exceptions.IncorrectRequestException;

import java.util.UUID;

/**
 * Main game instance class
 */
public class GameInstance {
    public final UUID gameId;
    public final GameField gameField;
    public final GameParams gameParams;
    @Getter private EnumGameStatus gameStatus;

    private GameInstance(UUID gameId, GameParams params) {
        this.gameId = gameId;
        this.gameParams = params;
        this.gameField = new GameField(this);
        this.gameStatus = EnumGameStatus.RUNNING;
    }

    /**
     * Constructor wrapper will be used instead of direct constructor call to prevent external ID declaring
     * @param fieldWidth field width
     * @param fieldHeight field height
     * @param minesCount amount of mines on field
     * @return new instance of this class
     * @throws IncorrectFieldParamsException when passed non-positive height/width/mines amount or if mines amount is too large for this field size
     */
    public static GameInstance create(int fieldWidth, int fieldHeight, int minesCount) throws IncorrectFieldParamsException {
        return new GameInstance(UUID.randomUUID(), new GameParams(fieldHeight, fieldWidth, minesCount));
    }

    /**
     * Method called when player interacted with field cell
     * @param col col index
     * @param row row index
     * @throws FieldAlreadyShownException when player tried to interact with cell which already shown
     * @throws FinalizedGameInteractionException when player tried to interact with field when game is over already
     * @throws IncorrectRequestException when player passed incorrect col or row index in request
     */
    public void interact(int col, int row) throws FieldAlreadyShownException, FinalizedGameInteractionException, IncorrectRequestException {
        if (gameStatus != EnumGameStatus.RUNNING)
            throw new FinalizedGameInteractionException();

        gameField.interact(col, row);
    }

    /**
     * Method used when game is over
     * @param isFailed is game failed or succeeded
     */
    public void finalizeGame(boolean isFailed) {
        this.gameStatus = isFailed ? EnumGameStatus.FAILED : EnumGameStatus.SUCCEEDED;
    }

    public boolean isCompleted() {
        return this.gameStatus != EnumGameStatus.RUNNING;
    }

    public boolean isFailed() {
        return this.gameStatus == EnumGameStatus.FAILED;
    }

    public enum EnumGameStatus {
        RUNNING,
        FAILED,
        SUCCEEDED
    }
}
