package ru.eugenegorbunoff.minesweeper.components;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.util.Pair;
import org.springframework.lang.Nullable;
import ru.eugenegorbunoff.minesweeper.exceptions.FieldAlreadyShownException;
import ru.eugenegorbunoff.minesweeper.exceptions.IncorrectRequestException;

import java.security.SecureRandom;
import java.util.*;

@Getter
public class GameField {

    private final GameInstance parent;

    private final FieldCell[][] gameFieldCell;
    private int freeFieldsLeft;

    /**
     * Simplified constructor which will use SecureRandom as random for main constructor
     * @param parent game instance connected to this game field
     */
    public GameField(GameInstance parent) {
        this(parent, new SecureRandom());
    }

    /**
     * Main class constructor
     * @param parent game instance connected to this game field
     * @param random random instance which will be used to shuffle mines on game field
     */
    public GameField(GameInstance parent, Random random) {
        this.parent = parent;

        GameParams params = parent.gameParams;
        final int fields = params.fieldWidth * params.fieldHeight;

        // creating one-dim list of cells with correct mined cells amount
        List<FieldCell> flatFieldCell = new ArrayList<>();
        for (int i = 0; i < fields; i++) {
            flatFieldCell.add(new FieldCell(i < params.minesCount));
        }

        // shuffling list of cells using given random (SecureRandom if you use simplified constructor)
        Collections.shuffle(flatFieldCell, random);

        // set the "free" fields count
        freeFieldsLeft = fields - params.minesCount;

        // creating bi-dim array of cells and unfolding one-dir list of cells into it
        gameFieldCell = new FieldCell[params.fieldHeight][params.fieldWidth];
        for (int i = 0; i < flatFieldCell.size(); i++) {
            gameFieldCell[i / params.fieldWidth][i % params.fieldWidth] = flatFieldCell.get(i);
        }

        // counting nearby mines amount for every cell
        for (int row = 0; row < gameFieldCell.length; row++) {
            for (int col = 0; col < gameFieldCell[row].length; col++) {
                short minesNearby = 0;

                for (int x = -1; x < 2; x++) {
                    for (int y = -1; y < 2; y++) {
                        if (x == y && x == 0)
                            continue;

                        FieldCell nearbyFieldCell = getField(col + x, row + y);
                        if (nearbyFieldCell != null && nearbyFieldCell.isMine)
                            minesNearby++;
                    }
                }

                gameFieldCell[row][col].setNearbyMines(minesNearby);
            }
        }
    }

    @Nullable private FieldCell getField(int col, int row) {
        if (row < 0 || row >= gameFieldCell.length)
            return null;

        FieldCell[] fieldCellRow = gameFieldCell[row];
        if (col < 0 || col >= fieldCellRow.length)
            return null;

        return fieldCellRow[col];
    }

    /**
     * Main field method controls interaction with cell
     * @param col cell col index
     * @param row cell row index
     * @throws FieldAlreadyShownException on clicked on already shown cell
     * @throws IncorrectRequestException on passed incorrect row or col param in request
     */
    public void interact(int col, int row) throws FieldAlreadyShownException, IncorrectRequestException {
        FieldCell fieldCell = getField(col, row);
        if (fieldCell == null)
            throw new IncorrectRequestException(String.format(
                    "Клетка [%d; %d] не существует на игровом поле {%d x %d}",
                    row, col, parent.gameParams.fieldHeight, parent.gameParams.fieldWidth
            ));

        if (fieldCell.isShown)
            throw new FieldAlreadyShownException(col, row);

        fieldCell.isShown = true;
        freeFieldsLeft--;

        if (fieldCell.isMine) {
            parent.finalizeGame(true);
            return;
        }

        if (fieldCell.nearbyMines == 0)
            showNearby(col, row, null);

        if (freeFieldsLeft <= 0) {
            parent.finalizeGame(false);
        }
    }

    /**
     * Recursively shows nearby cells on clicked on non-mine cell with 0 nearby mines
     * @param col cell col index
     * @param row cell row index
     * @param markedPositions set of cells marked to show. Null passes on first call represents method called not from recursion
     */
    private void showNearby(int col, int row, @Nullable Set<Pair<Integer, Integer>> markedPositions) {
        boolean firstCall = false;
        if (markedPositions == null) {
            markedPositions = new HashSet<>();
            firstCall = true;
        }

        // nearby cells positions
        Pair<Integer, Integer>[] nearbyCells = new Pair[] {
                Pair.of(col-1, row-1), Pair.of(col, row-1), Pair.of(col+1, row-1),
                Pair.of(col-1, row),                        Pair.of(col+1, row),
                Pair.of(col-1, row+1), Pair.of(col, row+1), Pair.of(col+1, row+1)
        };

        for (Pair<Integer, Integer> cell : nearbyCells) {
            if (!markedPositions.contains(cell)) {
                FieldCell fieldCell = getField(cell.getFirst(), cell.getSecond());
                if (fieldCell != null && !fieldCell.isMine) {
                    markedPositions.add(cell);
                    if (fieldCell.nearbyMines <= 0)
                        showNearby(cell.getFirst(), cell.getSecond(), markedPositions);
                }
            }
        }

        // we don't need to process cells on every recursion step so they will be processed on first-call stage
        if (firstCall) {
            for (Pair<Integer, Integer> markedPosition : markedPositions) {
                // this call of nullable method is safe because we already checked this position before
                getField(markedPosition.getFirst(), markedPosition.getSecond()).isShown = true;
                freeFieldsLeft--;
            }
        }
    }

    /**
     * Util method which collects game field into string bi-dim array (used for server response)
     * @return game field bi-dim string array representation
     */
    public String[][] getGameFieldAsArray() {
        return Arrays.stream(gameFieldCell)
                .map(col -> Arrays.stream(col)
                        .map(fieldCell -> fieldCell.getStringValue(parent.isFailed(), parent.isCompleted()))
                        .toArray(String[]::new)
                ).toArray(String[][]::new);
    }

    /**
     * Util class which contains information about field cell
     */
    @Getter @Setter
    public static class FieldCell {
        private short nearbyMines = 0;
        private boolean isShown = false;
        private final boolean isMine;

        public FieldCell(boolean isMine) {
            this.isMine = isMine;
        }

        /**
         *
         * @param isGameFailed game fail status defines if mines will be displayed as 'X' or as 'M'
         * @param isGameFinished game running status defines if all cells will be shown
         * @return string representation of current field cell
         */
        public String getStringValue(boolean isGameFailed, boolean isGameFinished) {
            if (!isShown() && !isGameFinished)
                return " ";
            if (isMine())
                return isGameFailed ? "X" : "M";
            return Integer.toString(nearbyMines);
        }
    }
}
