package ru.eugenegorbunoff.minesweeper;

import ru.eugenegorbunoff.minesweeper.components.GameInstance;
import ru.eugenegorbunoff.minesweeper.exceptions.GameAlreadyExistsException;
import ru.eugenegorbunoff.minesweeper.exceptions.IncorrectRequestException;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * This class stores all games running in this session
 */
public class GameHolder {
    private static final Map<UUID, GameInstance> GAMES = new HashMap<>();

    public static void addGame(UUID uuid, GameInstance instance) throws GameAlreadyExistsException {
        if (GAMES.containsKey(uuid))
            throw new GameAlreadyExistsException(uuid);

        GAMES.put(uuid, instance);
    }

    public static GameInstance getGame(UUID uuid) throws IncorrectRequestException {
        GameInstance game = GAMES.get(uuid);
        if (game == null)
            throw new IncorrectRequestException("Игра с указанным ID не найдена");
        return game;
    }
}
